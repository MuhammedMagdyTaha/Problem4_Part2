package com.company;


public class BankAccount {

    private String ID;
    private long balance;

    public BankAccount(String ID, long balance){
        this.ID = ID;
        this.balance = balance;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public long getBalance() {
        return balance;
    }

    private void setBalance(long balance) {
        this.balance = balance;
    }

    public boolean withdraw(long negativeBalance){

        if(this.balance >= negativeBalance){
            this.setBalance(this.getBalance() - negativeBalance);
            System.out.println("Your Balance Became: " + this.getBalance());
            return true;
        }else {
            System.out.println("Invalid Balance to Withdraw ..");
            return false;
        }
    }

//    public boolean addBalance(long addedBalance){
//        this.setBalance(this.getBalance() + addedBalance);
//        System.out.println("Your Balance Became: " + this.getBalance());
//        return true;
//    }
}
