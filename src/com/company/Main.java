package com.company;

public class Main {




    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount("1000", 500);
        BankAccountHandler handler = new BankAccountHandler(bankAccount);

        Thread husband = new Thread(() -> {
            System.out.println("Husband withdraw 100");
            handler.withdrawTogller(bankAccount);
        });

        Thread wife = new Thread(() -> {
            System.out.println("Wife withdraw 100");
            handler.withdrawTogller(bankAccount);
        });

        husband.start();
        wife.start();



    }
}
